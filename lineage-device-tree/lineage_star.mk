#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from star device
$(call inherit-product, device/xiaomi/star/device.mk)

PRODUCT_DEVICE := star
PRODUCT_NAME := lineage_star
PRODUCT_BRAND := Xiaomi
PRODUCT_MODEL := Star
PRODUCT_MANUFACTURER := xiaomi

PRODUCT_GMS_CLIENTID_BASE := android-xiaomi

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="star-user 12 RKQ1.211001.001 V13.0.2.0.SKAMIXM release-keys"

BUILD_FINGERPRINT := Xiaomi/star/star:12/RKQ1.211001.001/V13.0.2.0.SKAMIXM:user/release-keys
